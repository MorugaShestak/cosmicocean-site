const express = require('express');
const path = require('path');

const app = express();
const port = process.env.PORT || 3000;

app.use(express.static(path.join(__dirname, 'src')));

app.use((req, res, next) => {
    if (!req.path.includes('.')) {
        res.sendFile(path.join(__dirname, 'src', 'index.html'));
        res.sendFile(path.join(__dirname, 'src/pages', 'portfolio.html'));
        res.sendFile(path.join(__dirname, 'src/pages', 'about.html'));
    } else {
        next();
    }
});

app.listen(port, () => {
    console.log(`Сервер запущен на порту ${port}`);
});
