const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

gulp.task('sass', () => {
    return gulp.src('src/styles/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
});

gulp.task('watch', () => {
    browserSync.init({
        server: {
            baseDir: './src/'
        }
    });

    gulp.watch('src/styles/*.scss', gulp.series('sass'));
    gulp.watch('src/*.html').on('change', browserSync.reload);
});

gulp.task('default', gulp.series('watch'));
